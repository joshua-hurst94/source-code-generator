var express = require('express');
var app = express();
var path = require('path');
var formidable = require('express-formidable');
var fs = require('fs');
var mime = require('mime');
var exec = require('child_process').exec;

var uploadDir = path.join(__dirname, '/uploads');

app.use(express.static(path.join(__dirname, 'public')));
app.use(formidable({
  encoding: 'utf-8',
  uploadDir: uploadDir,
  multiples: true // support multiple file selection 
}));


app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'public/upload.html'));
});

app.post('/upload', function(req, res) {
  var num = req.fields.user; //the randomly generated 5 digit number
  var newFolder = path.join(uploadDir, "/" + num.toString());
  var fileString = "";
  var fileArray = new Array();
  fs.mkdirSync(newFolder); //creates a new folder inside of Uploads 
  var uploads = req.files.uploads;

  if (!Array.isArray(uploads)) {
    uploads = [uploads];
  }


  //Places the uploaded files into the 
  //Temporary folder
  for(var b = 0; b < uploads.length; ++b){
    var file = uploads[b];
    fileString += newFolder + "/" + file.name + " ";
    fs.renameSync(file.path, path.join(newFolder, file.name));
  }

  //Creates a pdf of the uploaded files
  //Upon success, deletes/unlinks each 
  //file inside the temp folder
  exec("a2ps " + fileString + "-o - | ps2pdf - " + newFolder + "/output.pdf", function(error, stdout, stderr){
    if(error){
      console.log("error: " + error.message);
      return;
    }
    for(var c = 0; c < uploads.length; ++c){
      fs.unlink(newFolder + "/" + uploads[c].name);
    }
    res.end('success');
  });  
});

// Allows downloading content that has been uploaded to server
// Example: /download?filename=foo.jpg
app.get('/download', function(req, res) {
  var name = req.query.filename;
  var user = req.query.user;
  var file = path.join(uploadDir, user.toString() + "/" + name);
  var filename = path.basename(file);
  var mimetype = mime.lookup(file);

  res.setHeader('Content-disposition', 'attachment; filename=' + filename);
  res.setHeader('Content-type', mimetype);
  res.sendFile(file, function(){
    //deletes the pdf file from server
    fs.unlink(file);
    //deletes the temporary directory
    fs.rmdir(uploadDir + "/" + user);
  });
  
  //var filestream = fs.createReadStream(file);
  //filestream.pipe(res);
});

var server = app.listen(3000, function(){
  console.log('Server listening on port 3000');
});
