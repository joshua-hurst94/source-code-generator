var fileArray = new Array();
var nameArray = new Array();
$(function () {
    //hides ajax loader gif when page loads
    $('#loading').hide();

    //disables the button and shows the loader
    //gif when the ajax request starts
    $(document).ajaxStart(function(){
      $("#btn").prop("disabled", true);
      $("#loading").show
    });

    //enables the button and hides the loader
    //gif when the ajax request stops
    $(document).ajaxStop(function(){
      $("#btn").prop("disabled", false);
      $("#loading").hide();
    });
});

$('.container').on({
    'dragover dragenter drop': function (e) {
      e.preventDefault();
      e.stopPropagation();
    }
});

$('.container').on('drop', function (e) {
    //resets the list of files and 
    //error message
    $('#fileList').html("");
    $('#filesize').html("");

    var dataTransfer = e.originalEvent.dataTransfer;
    console.log('Processing drop:', e.originalEvent.dataTransfer);

    if(dataTransfer.files.length > 0){
      for(var x = 0; x < dataTransfer.files.length; ++x){
        var file = dataTransfer.files[x];
        var filename = file.name.trim();
        //retrieves the size of each file in bytes
        //if too large, displays an error message
        //and does not save file
        var filesize = dataTransfer.files[x].size;
        if(filesize < 100000){
          fileArray.push(file);
          nameArray.push(file.name);
        }
        else{
          $('#filesize').html("File " + file.name + " is too large.");
        }
      }
    }
    //sorts the names with each upload
    nameArray.sort();

    //lists the filenames and creates
    //delete links next to them
    for(var r = 0; r < nameArray.length; ++r){
      var name = nameArray[r];
      $('#fileList').append('<li id="' + name + '">' + name + '  ' + '<a href="javascript:deleteLink(\'' + name + '\')">delete</li>');
    }
  });

$('#btn').on('click', function () {
    //calls uploadFiles and empties the fileArray
    uploadFiles(fileArray);
    fileArray.splice(0, fileArray.length);
    nameArray.splice(0, nameArray.length);
});

/*
 * Function: deleteLink
 * Purpose: removes the specified
 * filename from the list and
 * removes it from the fileArray.
 */
function deleteLink(obj) {
  var deleteChild;
  deleteChild = document.getElementById(obj);
  deleteChild.parentNode.removeChild(deleteChild);
  for(var w = 0; w < fileArray.length; ++w){
    if(obj == fileArray[w].name){
      fileArray.splice(w, 1);
    }
    if(obj == nameArray[w]){
      nameArray.splice(w, 1);
    }
  }
}

/*
 * Function: uploadFiles
 * Purpose: create a formData object
 * to be sent with the Ajax request 
 */
function uploadFiles(files) {
  if (files.length > 0) {
    // create a FormData object which will be sent as the data payload in the
    // AJAX request
    var formData = new FormData();

    // loop through all the selected files and add them to the formData object
    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      console.log("Will upload", file.name);
      // add the files to formData object for the data payload
      formData.append('uploads', file, file.name);
    }
    //Generates a random 5 digit number and uses 
    //that number for a temporary folder and
    //for the output pdf name.
    var num = Math.floor(Math.random()*90000) + 10000;
    formData.append('user', num);

    $.ajax({
      url: '/upload',
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success: function (data) {
        window.location.href = "/download?filename=output.pdf&user=" + num;
        $('#fileList').html("<p>PDF downloaded</p>");
      },
    });

  }
}
